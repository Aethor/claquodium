# Claquodium

__Claquodium - the new Medium experience__


# Usage

either :

* Test the extension 
  * type __about:debugging__ in the URL bar
  * click on __Load Temporary Add-on...__
  * select the __manifest.json__ file
* Install it 
  * type __about:config__ in the URL bar
  * search for __xpinstall.signatures.required__
  * toggle to False
  * run __make build__, this will create a __claquodium.zip__ file
  * go to firefox add ons management pane
  * __Install Add-on from file..._
  * choose the __claquodium.zip__ file
  
When done, just go to medium and... enjoy the **CLAC** !
