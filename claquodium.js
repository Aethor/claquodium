// Claqued SVG
let claquedSVG = `
<svg
   xmlns:osb="http://www.openswatchbook.org/uri/2009/osb"
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   sodipodi:docname="PINCE.svg"
   inkscape:version="1.0 (4035a4fb49, 2020-05-01)"
   id="svg8"
   version="1.1"
   viewBox="0 0 297 210"
   height="210mm"
   width="297mm">
  <defs
     id="defs2">
    <linearGradient
       osb:paint="solid"
       id="linearGradient2964">
      <stop
	 id="stop2962"
	 offset="0"
	 style="stop-color:#36442a;stop-opacity:1;" />
    </linearGradient>
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect966"
       effect="spiro" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect962"
       effect="spiro" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect958"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect954"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect931"
       effect="spiro" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect927"
       effect="spiro" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect923"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect919"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect915"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       effect="spiro"
       id="path-effect911"
       is_visible="true"
       lpeversion="1" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect903"
       effect="spiro" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect899"
       effect="spiro" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect895"
       effect="spiro" />
    <inkscape:path-effect
       lpeversion="1"
       is_visible="true"
       id="path-effect891"
       effect="spiro" />
  </defs>
  <sodipodi:namedview
     inkscape:window-maximized="1"
     inkscape:window-y="24"
     inkscape:window-x="0"
     inkscape:window-height="1029"
     inkscape:window-width="1920"
     showgrid="false"
     inkscape:document-rotation="0"
     inkscape:current-layer="layer1"
     inkscape:document-units="mm"
     inkscape:cy="366.59632"
     inkscape:cx="430.40263"
     inkscape:zoom="0.98994949"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0.0"
     borderopacity="1.0"
     bordercolor="#666666"
     pagecolor="#ffffff"
     id="base" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
	 rdf:about="">
	<dc:format>image/svg+xml</dc:format>
	<dc:type
	   rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
	<dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 2"
     id="layer2"
     inkscape:groupmode="layer">
    <g
       style="stroke-width:0.85388617;stroke-miterlimit:4;stroke-dasharray:none"
       transform="matrix(4.4503763,1.7806271,-1.7806271,4.4503763,-30.975464,-677.59838)"
       id="g2983">
      <g
	 transform="translate(7.6467224,-5.0978108)"
	 style="fill:none;fill-opacity:1;stroke-width:0.85388617;stroke-miterlimit:4;stroke-dasharray:none"
	 id="g2977" />
      <g
	 style="fill:none;fill-opacity:1;stroke-width:0.85388617;stroke-miterlimit:4;stroke-dasharray:none"
	 transform="rotate(-10.562604,70.113763,88.762064)"
	 id="g2981">
	<path
	   id="path2979"
	   style="fill:#fcfcfc;fill-opacity:1;stroke:#000000;stroke-width:0.85388617;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	   d="m 89.80138,123.14868 c -3.706569,1.09349 -7.255984,1.1825 -10.644102,3.04127 -3.549496,1.9473 -6.910005,4.3239 -9.568969,7.3769 -2.658966,3.05301 -4.590299,6.8249 -5.031993,10.84931 -0.204852,1.86647 -0.08868,3.76776 0.492568,4.26984 0.479384,-2.1629 1.030943,-2.9116 1.733237,-3.47373 0.550619,-0.44072 1.184391,-0.76371 1.772941,-1.15234 0.588549,-0.38863 1.149596,-0.86504 1.452107,-1.50216 0.26278,-0.55343 0.309133,-1.18032 0.38493,-1.78827 0.0758,-0.60794 0.196661,-1.23893 0.573764,-1.72178 0.3339,-0.42752 0.830987,-0.69032 1.316029,-0.93324 0.485041,-0.24291 0.987172,-0.48872 1.345833,-0.8957 0.363404,-0.41236 0.543919,-0.94965 0.726725,-1.468 0.182805,-0.51834 0.38842,-1.05157 0.785003,-1.43213 0.22609,-0.21695 0.505174,-0.3737 0.79888,-0.48289 0.293707,-0.10919 0.602607,-0.17237 0.912594,-0.21814 0.619976,-0.0915 1.256079,-0.11648 1.849698,-0.31739 0.674575,-0.22831 1.251987,-0.66906 1.844658,-1.06393 0.592671,-0.39487 1.242263,-0.7592 1.953293,-0.7994 0.461212,-0.0261 0.920829,0.0874 1.351566,0.25406 0.430645,0.16717 0.838189,0.38759 1.254763,0.58725 0.416573,0.19966 0.847561,0.38023 1.302555,0.46008 0.455012,0.0798 0.939324,0.053 1.355397,-0.14765 2.515852,-1.7048 2.809147,0.38012 3.792497,-0.18499 0.303551,-0.29565 0.645088,0.39322 1.682754,0.27443 0.857557,0.63612 0.837732,1.36473 0.574284,1.99507 -0.263449,0.63035 -0.767992,1.15637 -1.38681,1.44586 -0.444898,0.20813 -0.937006,0.29477 -1.426075,0.34017 -0.489069,0.0454 -0.981502,0.0517 -1.469265,0.1095 -1.135495,0.13451 -2.241736,0.55564 -3.152083,1.24753 -0.910347,0.69189 -1.618363,1.6573 -1.961614,2.748 -0.22099,0.70221 -0.290823,1.44234 -0.460728,2.15862 -0.169905,0.71628 -0.461073,1.43828 -1.020982,1.91622 -0.510978,0.43618 -1.185917,0.62029 -1.842016,0.7648 -0.656099,0.14451 -1.333691,0.2678 -1.909384,0.6141 -0.617688,0.37157 -1.062008,0.96582 -1.50462,1.53476 -0.442611,0.56894 -0.921566,1.14496 -1.5728,1.45398 -0.591615,0.28072 -1.271293,0.31302 -1.924194,0.26262 -0.6529,-0.0504 -1.299328,-0.17812 -1.95385,-0.19862 -1.43574,-0.0449 -2.837742,0.42728 -4.620608,1.21796 3.014696,1.2749 6.686977,1.70423 10.318972,1.47617 6.064764,-0.38083 12.017209,-2.63746 16.731027,-6.47234 2.476003,-2.01433 4.608464,-4.44985 6.278038,-7.17026 l 2.25596,-6.71055 -4.503931,-4.6685 z"
	   sodipodi:nodetypes="cssscsssssssssscssscsccccsssssssssssssscsscccc" />
      </g>
    </g>
  </g>
  <g
     id="layer1"
     inkscape:groupmode="layer"
     inkscape:label="Layer 1">
    <g
       id="g941" />
    <g
       style="stroke-width:0.85388618;stroke-miterlimit:4;stroke-dasharray:none"
       transform="matrix(4.7933789,0,0,4.7933789,-148.17111,-578.13062)"
       id="g2974">
      <g
	 id="g976"
	 style="fill:none;fill-opacity:1;stroke-width:0.85388618;stroke-miterlimit:4;stroke-dasharray:none"
	 transform="translate(7.6467224,-5.0978108)" />
      <g
	 id="g980"
	 transform="rotate(-10.562604,91.271117,238.53807)"
	 style="fill:none;fill-opacity:1;stroke-width:0.85388618;stroke-miterlimit:4;stroke-dasharray:none">
	<path
	   sodipodi:nodetypes="cssscsssssssssscssscscsssscsssssssssssssscsscccc"
	   d="m 91.381303,125.29677 c -3.706569,1.09349 -7.29506,2.58674 -10.683178,4.44551 -3.549496,1.9473 -6.910005,4.3239 -9.568969,7.3769 -2.658966,3.05301 -4.590299,6.8249 -5.031993,10.84931 -0.204852,1.86647 -0.08868,3.76776 0.492568,4.26984 0.479384,-2.1629 1.030943,-2.9116 1.733237,-3.47373 0.550619,-0.44072 1.184391,-0.76371 1.772941,-1.15234 0.588549,-0.38863 1.149596,-0.86504 1.452107,-1.50216 0.26278,-0.55343 0.309133,-1.18032 0.38493,-1.78827 0.0758,-0.60794 0.196661,-1.23893 0.573764,-1.72178 0.3339,-0.42752 0.830987,-0.69032 1.316029,-0.93324 0.485041,-0.24291 0.987172,-0.48872 1.345833,-0.8957 0.363404,-0.41236 0.543919,-0.94965 0.726725,-1.468 0.182805,-0.51834 0.38842,-1.05157 0.785003,-1.43213 0.22609,-0.21695 0.505174,-0.3737 0.79888,-0.48289 0.293707,-0.10919 0.602607,-0.17237 0.912594,-0.21814 0.619976,-0.0915 1.256079,-0.11648 1.849698,-0.31739 0.674575,-0.22831 1.251987,-0.66906 1.844658,-1.06393 0.592671,-0.39487 1.242263,-0.7592 1.953293,-0.7994 0.461212,-0.0261 0.920829,0.0874 1.351566,0.25406 0.430645,0.16717 0.838189,0.38759 1.254763,0.58725 0.416573,0.19966 0.847561,0.38023 1.302555,0.46008 0.455012,0.0798 0.939324,0.053 1.355397,-0.14765 0.273321,-0.13184 0.508508,-0.33405 0.705844,-0.56459 0.197333,-0.23052 0.358649,-0.48952 0.508474,-0.7534 0.299652,-0.52778 0.564406,-1.09022 0.999179,-1.51366 0.303551,-0.29565 0.683241,-0.51245 1.720907,-0.63124 0.857557,0.63612 0.837732,1.36473 0.574284,1.99507 -0.263449,0.63035 -0.767992,1.15637 -1.38681,1.44586 -0.444898,0.20813 -0.937006,0.29477 -1.426075,0.34017 -0.489069,0.0454 -0.981502,0.0517 -1.469265,0.1095 -1.135495,0.13451 -2.241736,0.55564 -3.152083,1.24753 -0.910347,0.69189 -1.618363,1.6573 -1.961614,2.748 -0.22099,0.70221 -0.290823,1.44234 -0.460728,2.15862 -0.169905,0.71628 -0.461073,1.43828 -1.020982,1.91622 -0.510978,0.43618 -1.185917,0.62029 -1.842016,0.7648 -0.656099,0.14451 -1.333691,0.2678 -1.909384,0.6141 -0.617688,0.37157 -1.062008,0.96582 -1.50462,1.53476 -0.442611,0.56894 -0.921566,1.14496 -1.5728,1.45398 -0.591615,0.28072 -1.271293,0.31302 -1.924194,0.26262 -0.6529,-0.0504 -1.299328,-0.17812 -1.95385,-0.19862 -1.43574,-0.0449 -2.837742,0.42728 -4.620608,1.21796 3.014696,1.2749 6.686977,1.70423 10.318972,1.47617 6.064764,-0.38083 12.017209,-2.63746 16.731027,-6.47234 2.476003,-2.01433 4.608464,-4.44985 6.278038,-7.17026 l 2.25596,-6.71055 -4.503931,-4.6685 z"
	   style="fill:#fcfcfc;fill-opacity:1;stroke:#000000;stroke-width:0.85388618;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	   id="path964" />
      </g>
    </g>
    <path
       id="path2992"
       style="fill:none;stroke:#000000;stroke-width:2.526;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       inkscape:transform-center-x="29.735328"
       inkscape:transform-center-y="-4.6289578"
       d="m 95.078361,189.69836 -0.55239,6.37832 0.18119,4.73192 -4.1885,-2.20902 -4.188651,-2.20903 4.006209,-2.52477 z"
       sodipodi:nodetypes="ccccccc" />
    <path
       sodipodi:nodetypes="ccccccc"
       d="m 86.717177,184.12155 -2.667132,5.82022 -1.421959,4.5168 -3.200628,-3.48995 -3.200589,-3.48988 4.622587,-1.02712 z"
       inkscape:transform-center-y="-10.592378"
       inkscape:transform-center-x="38.967572"
       style="fill:none;stroke:#000000;stroke-width:2.526;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       id="path2994" />
    <path
       id="path2996"
       style="fill:none;stroke:#000000;stroke-width:2.526;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       inkscape:transform-center-x="14.882791"
       inkscape:transform-center-y="-3.6707578"
       d="m 103.85096,190.90912 3.39295,5.42917 2.98828,3.67337 -4.67541,0.75131 -4.67537,0.75127 1.68708,-4.42468 z"
       sodipodi:nodetypes="ccccccc" />
  </g>
</svg>
`;

let clapSVGPaths = document.querySelectorAll('path[d^="M28.86"]');
for (let clapSVGPath of clapSVGPaths) {
    clapSVGPath.parentNode.parentNode.innerHTML = claquedSVG;
}

// Claqued *clac*
window.setInterval(() => {
    for (let element of document.getElementsByTagName("button")) {
	if (element.textContent.match(/^.*claps?$/)) {
	    element.textContent = element.textContent.replace(/claps?/, "*clac*");
	    console.log(element.textContent);
	    // let pasToiQuiDecide = element.cloneNode(true);
	    // pasToiQuiDecide.textContent = "*clac*";
	    // element.parentNode.appendChild(pasToiQuiDecide);
	    // console.log(element.parentNode.innerHTML);
	}
    }
}, 3000);
